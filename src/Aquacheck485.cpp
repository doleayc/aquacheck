#include "Aquacheck485.h"

Aquacheck485::Aquacheck485(Stream & serial, uint8_t readWritePin, bool isSoftwareSerial) :
    _serial(&serial),
    _readWritePin(readWritePin),
    _isSoftwareSerial(isSoftwareSerial)
{
}

Aquacheck485::Aquacheck485(Stream & serial, uint8_t readWritePin, char address, bool isSoftwareSerial) :
    _serial(&serial),
    _readWritePin(readWritePin),
    _isSoftwareSerial(isSoftwareSerial),
    AquacheckProbe(address)
{
}

void Aquacheck485::init() {
    if(_readWritePin > 0) {
        pinMode(_readWritePin, OUTPUT);
    }
}

void Aquacheck485::_calculateParityBits ( char * charArray ) {
    char * ptr = charArray;

    while (*ptr) {
        bool parity = false;

        for(byte i = 0; i<8; i++){
            parity ^= (*ptr >> i) & 0x1;
        }

        if ( parity ) {
            *ptr |= (byte)0x80;
        } else {
            *ptr &= (byte)0x7F;
        }

        *ptr++;
    }
}

void Aquacheck485::_streamWrite ( char * command ) {
    // If Software Serial is used, 8N1 is hardcoded on it. 
    // Sensor protocol needs 7E1 but 7E1 is the same as 8N1 without MSB(parity) bit.
    // As SoftwareSerial doesn't calculate parity bit, we need to do it.
    if(_isSoftwareSerial){
        _calculateParityBits(command);
    }

    if( _readWritePin > 0) {
        digitalWrite(_readWritePin, HIGH);
        _delay(25);
    }
  
    _serial->print(command);

    // At this moment, sometimes, HardwareSerial flush hangs. Search arduino forum and stackoverflow
    #if defined(__AVR__)
        _serial->flush();
    #endif

    if( _readWritePin > 0) {
        digitalWrite(_readWritePin, LOW);
    }
}

int Aquacheck485::_streamRead() const{
    /**
     * If any kind of Software Serial is used, 8N1 is hardcoded.
     * Sensor protocol needs 7E1 but 7E1 is the same as 8N1 without MSB(parity bit)
     * We ensure it with logic AND the byte read with 0x7F skipping MSB
     * */ 

    if(!_serial->available()) return -1;

    if(_isSoftwareSerial) {
        return (char)_serial->read() & 0x7F;
    } else {
        return _serial->read();
    }
}


