#include "AquacheckProbe.h"

AquacheckProbe::AquacheckProbe() {}

AquacheckProbe::AquacheckProbe(char address) : _address(address) {}

void AquacheckProbe::setCustomDelay( void (*f) (unsigned long)) {
    _delay = *f;
}

void AquacheckProbe::setDebugSerial( Stream &stream) {
    _debugSerial = &stream;
}

char AquacheckProbe::getSavedAddress() const {
    return _address;
}

char AquacheckProbe::findAddress() {

    _initializeStreamProtocol();
    _sendCommand(CMD_ASK_ADDRESS);

    _readLine();

    if( (_frameBuffer[0]>='0' && _frameBuffer[0]<='9') || 
        (_frameBuffer[0]>='A' && _frameBuffer[0]<='Z') || 
        (_frameBuffer[0]>='a' && _frameBuffer[0]<='z')) 
    {
        _address = _frameBuffer[0];
        return _address;
    } else {
        return '?';
    }

    _endStreamProtocol();    
}

void AquacheckProbe::getInfo(char * buffer, uint8_t size) {
    _initializeStreamProtocol();
    
    _sendCommand(CMD_INFO);

    _readLine(3000);

    if(buffer && size > 1) {
        strncpy(buffer, _frameBuffer, strlen(_frameBuffer) < size - 1 ? strlen(_frameBuffer) : size - 1);
    }

    _endStreamProtocol();
}

void AquacheckProbe::readHumidity(float * values) {
    if(values == nullptr) return;

    _initializeStreamProtocol();
    
    _sendCommand(CMD_READ_HUMIDITY_START);
    
    /**
     * Receive aTTTn response where:
     *  a = address
     *  TTT = number of seconds that reads will be available
     *  n = number of values (sensors)
     * */
    _readLine(3000);    

    /**
     * Receive address response. Sensor reads are availables
     * */
    _readLine(3000);

    _sendCommand(CMD_READ_FIRST_SENSORS);
    _readLine();
    _parseValues(values, 0);

    _sendCommand(CMD_READ_SECOND_SENSORS);
    _readLine();    
    _parseValues(values, 3);
}

void AquacheckProbe::readTemperature(float * values) {
    if(values == nullptr) return;

    _initializeStreamProtocol();

    _sendCommand(CMD_READ_TEMPERATURE_START);

    /**
     * Receive aTTTn. If any other read is started, TTT = 000
     * */
    _readLine(3000);

    _sendCommand(CMD_READ_FIRST_SENSORS);

    _readLine();
    _parseValues(values, 0);
    
    _sendCommand(CMD_READ_SECOND_SENSORS);
    _readLine(); 
    _parseValues(values, 3);
}


/** 
 * There are two different versions of probes.
 *  - Older version sends 3 values for every read. When one of the three expected sensor is not found, value is sent as 00.0000.
 *      Format four sensors probe: X+XXX.XXXX+000.000+000.0000\r\n
 *  - New version sends ONLY values from sensors that are presents.
 *      Format four sensors probe: X+XXX.XXXX\r\n 
 **/
void AquacheckProbe::_parseValues(float *values, byte startIndex) {
    if(strlen(_frameBuffer) < 1){
        values[startIndex] = -1.0;
        values[startIndex + 1] = -1.0;
        values[startIndex + 2] = -1.0;
    }
    else if(_frameBuffer[0] != _address) {
        values[startIndex] = -2.0;
        values[startIndex + 1] = -2.0;
        values[startIndex + 2] = -2.0;
    }
    else {   
        
        char *ptr = &_frameBuffer[1];
        size_t i=0;
        while (i < 3 && ptr != nullptr) {
            values[startIndex + i] = strtod(ptr, &ptr);
            ++i;
        }

        while ( i < 3 ) {
            values[startIndex + i] = 0.0;
            ++i;
        }
    }
}

void AquacheckProbe::_sendCommand(const char * command) {
    char cmd[5] = "";

    if(strncmp(command, CMD_ASK_ADDRESS, strlen(CMD_ASK_ADDRESS)) != 0) {
        cmd[0] = _address;
    } 

    strncat(cmd, command, strlen(command));
    strcat(cmd, "!");

    debugPrintLn(cmd);
    _streamWrite(cmd);
}

int AquacheckProbe::_readByte(uint32_t timeout) const {
    int c;
    uint32_t _startMillis = millis();

    do {
        c = _streamRead();

        if(c >= 0) {
            debugPrint((char)c);
            return c;
        }
        
    }while(millis() - _startMillis < timeout);

    return -1; //-1 indicates timeout;
}


size_t AquacheckProbe::_readBytesUntil(char terminator, char * buffer, size_t length, uint32_t timeout) {
    if(length < 1 ) {
        return 0;
    }

    size_t index = 0;

    while (index < length) {
        int c = _readByte(timeout);

        if(c < 0 || c == terminator) {
            break;
        }

        buffer[index] = static_cast<char>(c);
        ++index; 
    }

    if(index < length) {
        buffer[index] = '\0';
    }

    return index;
}

size_t AquacheckProbe::_readLine(char * buffer, size_t size, uint32_t timeout) {
    size_t len = _readBytesUntil(FRAME_TERMINATOR[FRAME_TERMINATOR_LENGTH - 1], buffer, size - 1, timeout);


    if( (FRAME_TERMINATOR_LENGTH > 1) && (buffer[len - (FRAME_TERMINATOR_LENGTH - 1)] == FRAME_TERMINATOR[0])) {
        len -= FRAME_TERMINATOR_LENGTH - 1;
    }

    buffer[len] = '\0';
    
    // Without this brief delay 485 probes aren't read well with 485 adapters... Why??
    _delay(50);
    return len;
}

size_t AquacheckProbe::_readLine() {
    return _readLine(_frameBuffer, FRAME_BUFFER_SIZE);
}

size_t AquacheckProbe::_readLine(uint32_t timeout) {
    return _readLine(_frameBuffer, FRAME_BUFFER_SIZE, timeout);
}