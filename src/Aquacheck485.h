#pragma once

#include "AquacheckProbe.h"

class Aquacheck485 : public AquacheckProbe {
    public:
        Aquacheck485 (Stream & serial, uint8_t readWritePin, bool isSoftwareSerial = false);
        Aquacheck485 (Stream & serial, uint8_t readWritePin, char address, bool isSoftwareSerial = false);
        ~Aquacheck485(){};

        void init();

    private:
        Stream * _serial;
        bool _isSoftwareSerial = false;
        uint8_t _readWritePin;        

        void _calculateParityBits(char * charArray);

        // Override AquacheckProbe
        void _streamWrite( char * command );
        int _streamRead() const;
    
};