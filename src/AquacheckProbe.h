#pragma once

#include <Arduino.h>
#include <stdint.h>

#define debugPrintLn(...) { if (this->_debugSerial) this->_debugSerial->println(__VA_ARGS__); }
#define debugPrint(...) { if (this->_debugSerial) this->_debugSerial->print(__VA_ARGS__); }

#define FRAME_BUFFER_SIZE   64
#define FRAME_TERMINATOR        "\r\n"
#define FRAME_TERMINATOR_LENGTH (sizeof(FRAME_TERMINATOR) - 1)      // Length of CRLF without '\0' terminator

#define CMD_ASK_ADDRESS             "?"
#define CMD_INFO                    "I"
#define CMD_READ_HUMIDITY_START     "M"
#define CMD_READ_TEMPERATURE_START  "M1"
#define CMD_READ_FIRST_SENSORS      "D0"
#define CMD_READ_SECOND_SENSORS     "D1"

class AquacheckProbe {
    public:
        AquacheckProbe();
        AquacheckProbe(char address);

        virtual void init() {}
        char getSavedAddress() const;
        void getInfo (char * buffer, uint8_t size);
        char findAddress ();
        void readHumidity (float * values);
        void readTemperature (float * values);
        void setCustomDelay (void (*f)(unsigned long));
        void setDebugSerial (Stream &stream);

    protected:
        char _address;
        Stream * _debugSerial = nullptr;

        void (*_delay)(unsigned long) = delay;


    private:
        char _frameBuffer[FRAME_BUFFER_SIZE] = "";  

        // ----- Pure virtual
        virtual void _streamWrite(char * command) = 0;
        virtual int _streamRead() const = 0;        
        // ------

        virtual void _initializeStreamProtocol() const {};
        virtual void _endStreamProtocol() const {};
        virtual void _sendCommand(const char * command);
        int _readByte(uint32_t timeout) const;
        size_t _readBytesUntil(char terminator, char * buffer, size_t length, uint32_t timeout);
        size_t _readLine(char * buffer, size_t length, uint32_t timeout = 2000);
        
        size_t _readLine();
        size_t _readLine(uint32_t timeout);
        void _parseValues(float * values, uint8_t startIndex);
};