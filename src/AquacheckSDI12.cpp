#include "AquacheckSDI12.h"



AquacheckSDI12::AquacheckSDI12 (SDI12 *sdiInstance) :
    _sdi(sdiInstance)
    {
        
    }

AquacheckSDI12::AquacheckSDI12 (SDI12 &sdiInstance) :
    _sdi(&sdiInstance)
{
}

AquacheckSDI12::AquacheckSDI12 (SDI12 &sdiInstance, char address) :
    _sdi(&sdiInstance),
    AquacheckProbe(address)
{
}

void AquacheckSDI12::_initializeStreamProtocol() const {
    _sdi->begin();
    _delay(500);
}

void AquacheckSDI12::_endStreamProtocol() const {
    _sdi->end();
}

void AquacheckSDI12::_streamWrite(char * command) {
    _sdi->sendCommand(command);
    _sdi->flush();
}

int AquacheckSDI12::_streamRead() const{
    return _sdi->read();
}