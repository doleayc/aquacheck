#pragma once

#include "AquacheckProbe.h"
#include "SDI12.h"



class AquacheckSDI12 : public AquacheckProbe
{
    public:
        AquacheckSDI12(SDI12 *sdiInstance);
        AquacheckSDI12(SDI12 &sdiInstance);
        AquacheckSDI12(SDI12 &sdiInstance, char address);
        ~AquacheckSDI12(){};

    private:
        SDI12 * _sdi;

        // ----- Override AquacheckProbe
        void _initializeStreamProtocol() const;
        void _endStreamProtocol() const;
        void _streamWrite(char * command);
        int _streamRead() const;

    
};