#include "AquacheckSDI12.h"
#include "EnableInterrupt.h"

#define ENABLE_AQUACHECK_DEBUG  1
#define debugSerial Serial

#define PIN_SDI12 5


SDI12 sdi (PIN_SDI12);
AquacheckSDI12 aquacheck( sdi );

void setup() {
  // put your setup code here, to run once:
  debugSerial.begin(19200);

  // If SDI pin hasn't dedicated interrupt as D2 on Arduino UNO, we should use any other digital pin.
  // Then, we need to set an event on PIN_CHANGE_INTERRUPT on this pin.
  // If SDI pin has own interrupt, we must use it.
  enableInterrupt(PIN_SDI12, sdi.handleInterrupt, CHANGE);

  #if ENABLE_AQUACHECK_DEBUG == 1
    aquacheck.setDebugSerial(debugSerial);
  #endif
  
  debugSerial.println("START");
  
  char address;
  
  do {    
    address = aquacheck.findAddress();
    
    debugSerial.print(F("Address: "));
    
    if(address == '?') {
      debugSerial.println(F("Unable to get"));
      delay(1000);
    }
    else {
      debugSerial.println(address);
    }
  }while(address == '?');

  aquacheck.getInfo(nullptr, 0);

  delay(1000);
}

void loop() {
  // put your main code here, to run repeatedly:

  float humidity[6];
  float temperature[6];

  debugSerial.println(F("HUMIDITY:"));
  aquacheck.readHumidity(humidity);

  printValues(humidity);
    
  delay(1000);
 
  debugSerial.println(F("Temperature:"));
  aquacheck.readTemperature(temperature); 

  printValues(temperature);
  delay(5000);
}

void printValues(float * values) {
  debugSerial.println(F("Values: "));
  
  for(size_t i = 0; i < 6; ++i) {
    debugSerial.println(values[i]);  
  }
}
