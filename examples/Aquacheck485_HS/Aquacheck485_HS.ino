#include "Aquacheck485.h"

#define ENABLE_AQUACHECK_DEBUG  1
#define debugSerial             Serial
#define aquacheckSerial         Serial1
#define EN485_PIN   0     // Define EN485_PIN: 0 if no enable is needed, pin number otherwise


Aquacheck485 aquacheck(aquacheckSerial, EN485_PIN);

void setup() {
  // put your setup code here, to run once:
  debugSerial.begin(19200);
  aquacheckSerial.begin(1200, SERIAL_7E1);

  #if ENABLE_AQUACHECK_DEBUG == 1
    aquacheck.setDebugSerial(debugSerial);
  #endif
  
  aquacheck.init();
  
  debugSerial.println("START");
  
  char address;
  
  do {    
    address = aquacheck.findAddress();
    
    debugSerial.print(F("Address: "));
    
    if(address == '?') {
      debugSerial.println(F("Unable to get"));
      delay(1000);
    }
    else {
      debugSerial.println(address);
    }
  }while(address == '?');

  aquacheck.getInfo(nullptr, 0);

  delay(1000);
   
}

void loop() {
  // put your main code here, to run repeatedly:  
  float humidity[6];
  float temperature[6];

  debugSerial.println(F("HUMIDITY:"));
  aquacheck.readHumidity(humidity);

  printValues(humidity);
    
  delay(1000);
 
  debugSerial.println(F("Temperature:"));
  aquacheck.readTemperature(temperature); 

  printValues(temperature);
  delay(5000);
}

void printValues(float * values) {
  debugSerial.println(F("Values: "));
  
  for(size_t i = 0; i < 6; ++i) {
    debugSerial.println(values[i]);  
  }
}
