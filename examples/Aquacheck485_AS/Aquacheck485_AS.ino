#include "Aquacheck485.h"
#include "AltSoftSerial.h"

#define ENABLE_AQUACHECK_DEBUG  1
#define debugSerial             Serial

#define EN485_PIN   4     // Define EN485_PIN: 0 if no enable is needed, pin number otherwise

AltSoftSerial altSerial;
Aquacheck485 aquacheck(altSerial, EN485_PIN, true);

void setup() {
  // put your setup code here, to run once:
  debugSerial.begin(19200);
  altSerial.begin(1200);

  aquacheck.init();

  #if ENABLE_AQUACHECK_DEBUG == 1
    aquacheck.setDebugSerial(debugSerial);
  #endif
  
  debugSerial.println("START");
  
  char address;
  
  do {    
    address = aquacheck.findAddress();
    
    debugSerial.print(F("Address: "));
    
    if(address == '?') {
      debugSerial.println(F("Unable to get"));
      delay(1000);
    }
    else {
      debugSerial.println(address);
    }
  }while(address == '?');

  aquacheck.getInfo(nullptr, 0);

  delay(1000);
}

void loop() {
  float humidity[6];
  float temperature[6];

  debugSerial.println(F("HUMIDITY:"));
  aquacheck.readHumidity(humidity);

  printValues(humidity);
    
  delay(1000);
 
  debugSerial.println(F("Temperature:"));
  aquacheck.readTemperature(temperature); 

  printValues(temperature);
  delay(5000);
}

void printValues(float * values) {
  debugSerial.println(F("Values: "));
  
  for(size_t i = 0; i < 6; ++i) {
    debugSerial.println(values[i]);  
  }
}
